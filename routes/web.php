<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/post', [PostController::class, 'index'])->name('post');
Route::post('/create-post', [PostController::class, 'store'])->name('post.save');


// Create Client
    // Route::get('client',[ClientController::class,'index'])->name('client');
    // Route::get('client/create',[ClientController::class,'create'])->name('client.create');
    // Route::get('client/{client}/edit',[ClientController::class,'edit'])->name('client.edit');
    // Route::post('client/store',[ClientController::class,'store'])->name('client.store');
    // Route::put('client/{client}',[ClientController::class,'update'])->name('client.update');
    // Route::post('client/{client}',[ClientController::class,'destroy'])->name('client.destroy');

    Route::resource('clients', ClientController::class);

// Manage Contact
Route::get('contact',[ContactController::class,'index'])->name('contact');
Route::get('contact/create',[ContactController::class,'create'])->name('contact.create');
Route::post('contact/store',[ContactController::class,'store'])->name('contact.store');

// Configure for File Manager
Route::group(['prefix' => 'filemanager', 'middleware' => ['web']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

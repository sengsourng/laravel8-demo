<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        $contacts = Contact::latest()->get();
        return view('contacts.index', compact('contacts'));
    }

    public function create()
    {

        return view('contacts.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $contact = Contact::create($input);

        if($request->hasFile('avatar') && $request->file('avatar')->isValid()){
            $contact->addMediaFromRequest('avatar')->toMediaCollection('contact');
        }

        return redirect()->route('contact');
    }
}

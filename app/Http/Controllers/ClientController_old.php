<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::latest()->get();
        return view('clients.index', compact('clients'));
    }

    public function create()
    {

        return view('clients.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $client = Client::create($input);

        if($request->hasFile('avatar') && $request->file('avatar')->isValid()){

            $client->addMediaFromRequest('avatar')->toMediaCollection('avatar');
        }
        return redirect()->route('client');
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        // $input = $request->all();

        $image = Client::find($id);
        $image->name = $request->name;
        $image->save();

        if ($image) {
            if ($request->hasFile('image')) {
                $image->clearMediaCollection('client');
                $image->addMediaFromRequest('image')->toMediaCollection('client');
            }
        }

        session()->flash('success', 'Image Update successfully');

        return redirect()->route('client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Client::find($id);
        $image->delete();

        session()->flash('success', 'Image Delete successfully');

        return redirect()->route('client.index');
    }
}

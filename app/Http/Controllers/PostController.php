<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
    	$posts = Post::orderBy('id', 'DESC')->get();
        // dd($posts);
        return view('index', compact('posts'));
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'title_name' => 'required',
            'content' => 'required',
            'tags' => 'required',
            'image' => 'required',
        ]);

    	$input = $request->all();
    	$tags = explode(",", $request->tags);

    	$post = Post::create($input);
    	$post->tag($tags);

        if($request->hasFile('image') && $request->file('image')->isValid()){
            $post->addMediaFromRequest('image')->toMediaCollection('post');
        }

        return back()->with('success','Post added to database.');
    }
}

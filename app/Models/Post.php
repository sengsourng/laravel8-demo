<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model implements HasMedia
{

    use HasFactory, InteractsWithMedia;
    use \Conner\Tagging\Taggable;

    protected $fillable = [
        'title_name',
        'content'
    ];
}

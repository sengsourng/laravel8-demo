<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Integrate Spatie Medialibrary in Laravel</title>
    @include('includes.custom_header')

</head>

<body>

  @include('includes.navbar')


    <div class="container">
        <div class="row row-cols-1 row-cols-md-4 g-3 mt-4">
            <div class="col-md-12">
                <div class="float-start">
                    <h3>List of Contact </h3>
                </div>
                <div class="float-end">
                    <a href="{{ route('contact.create') }}" class="btn btn-primary">Create Contact</a>
                </div>
            </div>

            @foreach($contacts as $key=>$item)
            <div class="col">
                <div class="card mb-2" style="max-width: 540px;">
                    <div class="row g-0">
                      <div class="col-md-4">
                        <img class="img-contact" src="{{$item->getFirstMediaUrl('contact', 'thumb')}}" class="img-fluid rounded-start" alt="{{ $item->name }}">
                      </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h6 class="card-title">{{ $item->name }}</h6>
                          <small>{{ $item->email }}</small>
                          <p>{{ $item->phone }}</p>
                            <p class="card-text">
                              <small class="text-muted">{{ \Carbon\Carbon::parse($item->updated_at)->diffForhumans() }}</small>
                            </p>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            @endforeach
        </div>
    </div>
</body>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>

</html>

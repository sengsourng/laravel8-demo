 <header>
    <div class="bg-dark">
        <div class="container-fluid d-none d-lg-block">
            <div class="row justify-content-md-center pt-2">
                <div class="col-4" style="text-align: center;">
                    <img height="96px" src="{{asset('images/sourng_web_dev.png')}}">
                </div>
                <div class="col-8"><img src="{{asset('images/banner_top.jpg')}}" alt=""></div>
            </div>
        </div>
    </div>

    <nav id="navbar_top" class="navbar navbar-expand-lg navbar-dark bg-info banner">
     <div class="container">
          <a class="navbar-brand" href="/"><img height="34px" src="{{asset('images/sourng_web_dev.png')}}" alt=""></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav">
          <span class="navbar-toggler-icon"></span>
        </button>
      <div class="collapse navbar-collapse" id="main_nav">
        <ul class="navbar-nav ms-auto">
            <li class="nav-item">
                <a class="nav-link {{ (request()->is('post')) ? 'active' : '' }}" aria-current="page" href="{{route('post')}}">Posts</a>
              </li>
              <li class="nav-item">
                {{-- <a class="nav-link {{ (request()->is('client')) ? 'active' : '' }}" aria-current="page" href="{{route('client')}}">Clients</a> --}}
                <a class="nav-link {{ (request()->is('clients')) ? 'active' : '' }}" href="{{route('clients.index')}}">Clients</a>
            </li>
              <li class="nav-item">
                <a class="nav-link {{ (request()->is('contact')) ? 'active' : '' }}" href="{{route('contact')}}">Contact</a>
            </li>

        </ul>
      </div> <!-- navbar-collapse.// -->
     </div> <!-- container-fluid.// -->
    </nav>
    </header>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"
    rel="stylesheet" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Hanuman:wght@300;400;700;900&display=swap" rel="stylesheet">

    <link rel="shortcut icon" type="image/png" href="{{ asset('vendor/laravel-filemanager/img/72px color.png') }}">

<style>
    .bootstrap-tagsinput .tag {
        margin-right: 2px;
        color: #ffffff;
        background: #2196f3;
        padding: 3px 7px;
        border-radius: 3px;
    }

    .bootstrap-tagsinput {
        width: 100%;
    }

    .limited-text{
        white-space: nowrap;
        width: 100%;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .limited-title{
        white-space: nowrap;
        width: 100%;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .img-post {
        width: 100%;
        height: 145px;
        object-fit: cover;
        object-position: bottom;
    }
    body{
        font-family: 'Hanuman', serif !important;
        font-weight: 400;
    }
    .card-title{
        font-family: 'Hanuman', serif !important;
        font-weight: 700;
    }


    /* For Scrolling */

    /* width */
        ::-webkit-scrollbar {
        width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 10px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
        background: rgb(2, 144, 68);
        border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
        background: #b30000;
        }



        /* Menu Custom */
        .nav-pills .nav-link {
            background: 0 0 !important;

            border: 0;
            border-radius: 0px !important;
            /* color: white !important; */
        }

        .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
            color: rgb(253, 251, 251);
            /* background-color: #0d6efd; */
            background: 0 0 !important;

        }

        .img-contact {
            width: 100%;
            height: 145px;
            object-fit: cover;
            object-position: bottom;
        }

        .banner {
            align-items: center;
            background-color: #002ead !important;
            color: var(--gray00);
            /* display: flex;
            font-size: 14px;
            font-weight: 700;
            justify-content: center;
            padding: 8px;
            position: fixed;
            text-decoration: none;
            top: var(--header-height);
            width: 100vw; */
            z-index: 999;
        }
        .image-upload > input {
            visibility:hidden;
            width:0;
            height:0
        }

</style>


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel 8 Tags System Example</title>
    @include('includes.custom_header')
</head>

<body class="bg-default">

    @include('includes.navbar')

    <div class="container">

        <div class="row justify-content-md-center pt-4">
            <div class="col-md-5">
                <h4>Add New Post</h4>
                    @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        @php
                        Session::forget('success');
                        @endphp
                    </div>
                    @endif

                    <form action="{{ url('create-post') }}" enctype="multipart/form-data" method="post">
                        {{ csrf_field() }}
                        <div class="mb-3">
                            <input type="text" class="form-control" name="title_name" placeholder="Enter title">
                            @if ($errors->has('title_name'))
                            <span class="text-danger">{{ $errors->first('<title></title>') }}</span>
                            @endif
                        </div>


                        <div class="mb-3">
                            <textarea class="form-control" name="content" rows="6" placeholder="Enter content"></textarea>
                            @if ($errors->has('content'))
                            <span class="text-danger">{{ $errors->first('content') }}</span>
                            @endif
                        </div>


                        <div class="mb-3">
                            <input class="form-control" type="text" data-role="tagsinput" name="tags">
                            @if ($errors->has('tags'))
                            <span class="text-danger">{{ $errors->first('tags') }}</span>
                            @endif
                        </div>

                        <div class="mb-3">
                            <div class="image-upload  pb-2" style="text-align: center; cursor: pointer; ">
                                <label for="file-input">
                                  <img id="blah" style="width: 200px" src="{{asset('images/upload_image.png')}}" style="pointer-events: none"/>
                                </label>

                                {{-- <input id="file-input" type="file" /> --}}
                                <input id="file-input" type="file" name="image" class="form-control" onchange="readURL(this);">
                              </div>

                            @if ($errors->has('image'))
								<span class="text-danger">{{ $errors->first('image') }}</span>
                            @endif

                        </div>


                        <div class="d-grid">
                            <button class="btn btn-info btn-submit">ដាក់ផ្សាយ</button>
                        </div>
                    </form>



                    <div class="container">
                        <h1 class="page-header">Integration Demo Page</h1>
                        <div class="row">
                          <div class="col-md-6">
                            <h2 class="mt-4">CKEditor</h2>
                            <textarea name="ce" class="form-control"></textarea>
                          </div>
                          <div class="col-md-6">
                            <h2 class="mt-4">TinyMCE</h2>
                            <textarea name="tm" class="form-control"></textarea>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <h2 class="mt-4">Summernote</h2>
                            <textarea id="summernote-editor" name="content"></textarea>
                          </div>
                          <div class="col-md-6">
                            <h2 class="mt-4">Standalone Image Button</h2>
                            <div class="input-group">
                              <span class="input-group-btn">
                                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary text-white">
                                  <i class="fa fa-picture-o"></i> Choose
                                </a>
                              </span>
                              <input id="thumbnail" class="form-control" type="text" name="filepath">
                            </div>
                            <div id="holder" style="margin-top:15px;max-height:100px;"></div>
                            <h2 class="mt-4">Standalone File Button</h2>
                            <div class="input-group">
                              <span class="input-group-btn">
                                <a id="lfm2" data-input="thumbnail2" data-preview="holder2" class="btn btn-primary text-white">
                                  <i class="fa fa-picture-o"></i> Choose
                                </a>
                              </span>
                              <input id="thumbnail2" class="form-control" type="text" name="filepath">
                            </div>
                            <div id="holder2" style="margin-top:15px;max-height:100px;"></div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <h2 class="mt-4">Embed file manager</h2>
                            <iframe src="/filemanager" style="width: 100%; height: 500px; overflow: hidden; border: none;"></iframe>
                          </div>
                        </div>
                    </div>




        </div>
        <div class="col-md-7 alert alert-primary">
            <div class="mt-2 text-center">
                <h4>បញ្ជីពត៌មាន បានផ្សាយ</h4>
            </div>

            <div class="row g-1" style="
                                    width: 100%;
                                    height: 800px;
                                    overflow: auto;
                                ">
                @if($posts->count())
                    @foreach($posts as $key => $post)

                   <div class="col-md-12">
                    <div class="card" style="max-width: 740px;">
                        <div class="row g-0">
                          <div class="col-md-4">
                            @if ($post->getFirstMediaUrl('post', 'thumb'))
                                <img src="{{$post->getFirstMediaUrl('post', 'thumb')}}" class="img-post" alt="...">
                            @else
                                <img src="{{asset('images/upload_image.png')}}" style="width: 60%;" alt="">
                            @endif
                          </div>
                          <div class="col-md-8">
                            <div class="card-body">
                              <p class="card-title limited-title">{{ $post->title_name }}</p>
                                <div class="card-text limited-text">
                                    {{ $post->content }}
                                </div>

                                <div>
                                    <strong>Tag:</strong>
                                    @foreach($post->tags as $tag)
                                        {{-- <label class="label label-info">{{ $tag->name }}</label> --}}
                                        <span class="badge bg-primary">{{ $tag->name }}</span>
                                    @endforeach
                                </div>

                                <small class="text-muted">{{ \Carbon\Carbon::parse($post->updated_at)->diffForhumans() }}</small>

                                <div class="float-end">
                                    <a href="{{ route('contact.edit') }}" class="btn btn-success btn-sm">Edit</a>
                                    <a href="{{ route('contact.destroy') }}" class="btn btn-danger btn-sm">Delete</a>



                                 </div>
                            </div>
                          </div>
                        </div>
                    </div>
                   </div>

                    @endforeach
                @endif
            </div>
        </div>

    </div>


    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>


    <script>
        document.addEventListener("DOMContentLoaded", function(){
            window.addEventListener('scroll', function() {
                if (window.scrollY > 50) {
                    document.getElementById('navbar_top').classList.add('fixed-top');
                    // add padding top to show content behind navbar
                    navbar_height = document.querySelector('.navbar').offsetHeight;
                    document.body.style.paddingTop = navbar_height + 'px';
                } else {
                    document.getElementById('navbar_top').classList.remove('fixed-top');
                    // remove padding top from body
                    document.body.style.paddingTop = '0';
                }
            });
        });



        // បង្ហាញរូបភាពបន្ទាប់ពីទាញ
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>








<script>
    var route_prefix = "/filemanager";
   </script>

   <!-- CKEditor init -->
   <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/ckeditor.js"></script>
   <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/adapters/jquery.js"></script>
   <script>
     $('textarea[name=ce]').ckeditor({
       height: 100,
       filebrowserImageBrowseUrl: route_prefix + '?type=Images',
       filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
       filebrowserBrowseUrl: route_prefix + '?type=Files',
       filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
     });
   </script>

   <!-- TinyMCE init -->
   <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
   <script>
     var editor_config = {
       path_absolute : "",
       selector: "textarea[name=tm]",
       plugins: [
         "link image"
       ],
       relative_urls: false,
       height: 129,
       file_browser_callback : function(field_name, url, type, win) {
         var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
         var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
         var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
         if (type == 'image') {
           cmsURL = cmsURL + "&type=Images";
         } else {
           cmsURL = cmsURL + "&type=Files";
         }
         tinyMCE.activeEditor.windowManager.open({
           file : cmsURL,
           title : 'Filemanager',
           width : x * 0.8,
           height : y * 0.8,
           resizable : "yes",
           close_previous : "no"
         });
       }
     };
     tinymce.init(editor_config);
   </script>

   <script>
     {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
   </script>
   <script>
     $('#lfm').filemanager('image', {prefix: route_prefix});
     // $('#lfm').filemanager('file', {prefix: route_prefix});
   </script>

   <script>
     var lfm = function(id, type, options) {
       let button = document.getElementById(id);
       button.addEventListener('click', function () {
         var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
         var target_input = document.getElementById(button.getAttribute('data-input'));
         var target_preview = document.getElementById(button.getAttribute('data-preview'));
         window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
         window.SetUrl = function (items) {
           var file_path = items.map(function (item) {
             return item.url;
           }).join(',');
           // set the value of the desired input to image url
           target_input.value = file_path;
           target_input.dispatchEvent(new Event('change'));
           // clear previous preview
           target_preview.innerHtml = '';
           // set or change the preview image src
           items.forEach(function (item) {
             let img = document.createElement('img')
             img.setAttribute('style', 'height: 5rem')
             img.setAttribute('src', item.thumb_url)
             target_preview.appendChild(img);
           });
           // trigger change event
           target_preview.dispatchEvent(new Event('change'));
         };
       });
     };
     lfm('lfm2', 'file', {prefix: route_prefix});
   </script>



   <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
   <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
   <style>
     .popover {
       top: auto;
       left: auto;
     }
   </style>
   <script>
     $(document).ready(function(){
       // Define function to open filemanager window
       var lfm = function(options, cb) {
         var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
         window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
         window.SetUrl = cb;
       };
       // Define LFM summernote button
       var LFMButton = function(context) {
         var ui = $.summernote.ui;
         var button = ui.button({
           contents: '<i class="note-icon-picture"></i> ',
           tooltip: 'Insert image with filemanager',
           click: function() {
             lfm({type: 'image', prefix: '/filemanager'}, function(lfmItems, path) {
               lfmItems.forEach(function (lfmItem) {
                 context.invoke('insertImage', lfmItem.url);
               });
             });
           }
         });
         return button.render();
       };
       // Initialize summernote with LFM button in the popover button group
       // Please note that you can add this button to any other button group you'd like
       $('#summernote-editor').summernote({
         toolbar: [
           ['popovers', ['lfm']],
         ],
         buttons: {
           lfm: LFMButton
         }
       })
     });
   </script>
</body>

</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add Spatie Medialibrary in Laravel</title>
    @include('includes.custom_header')
</head>

<body class="bg-white">
    @include('includes.navbar')

    <div class="container text-black">
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <div class="d-flex p-2 bg-white mb-3">
                    <a href="{{ route('clients.index') }}" class="btn btn-primary btn-sm">Go Back</a>
                </div>
                <div>
                    <h3>Edit Update Client</h3>
                    <form action="{{ route('clients.update',$client->id) }}" enctype="multipart/form-data" method="post">
                        @csrf
                        @method('put')

                        <div class="mb-3">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{$client->name}}">
                        </div>
                        <div class="mb-3">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" value="{{$client->email}}">
                        </div>
                        <div class="mb-3">
                            <label>Image:</label>
                            <input type="file" name="avatar" class="form-control">
                        </div>
                        <div class="d-grid">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>



    </div>
</body>

</html>
